const pg = require('./pg-config')
const {readdir, readFile} = require('fs/promises')

async function* readFiles(path) {
	path = process.cwd() + path
	const files = (await readdir(path))
		.sort((a, b) => new Date(a.split('|')[0]) > new Date(b.split('|')[0]) ? 1 : -1)
	while(files.length > 0){
		const fileName = files.shift()
		const sql = await readFile(path + fileName, 'utf8') 
		yield [sql, fileName]	
	}
}

const path = "/sql/";

(async () => {
	for await (const [sql, fileName] of readFiles(path)) {
		try{
			const queries = sql.split('\n\n')
			for (const query of queries){
				await pg.query(query)
			}
			console.log(`Migration ${fileName} success`)
		} catch(e) {
			console.error(`ERROR: Migration file ${fileName} failed`)
			console.error(e)
		}
	}
})();
