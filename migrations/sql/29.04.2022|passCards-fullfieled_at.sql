ALTER TABLE Requests ADD COLUMN IF NOT EXISTS fulfilled_at timestamp;
DO $$ BEGIN
    CREATE TYPE passCard AS (
    type text,
    id text
);
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

ALTER TABLE Requests ADD COLUMN IF NOT EXISTS pass passCard;

