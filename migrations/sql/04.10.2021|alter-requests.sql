do $$
begin
	alter table requests rename accepted_at to reviewed_at;
	exception when others then null;
end;
$$;

do $$
begin
	alter table requests      
	alter COLUMN accepted DROP DEFAULT,
	alter COLUMN accepted TYPE integer USING case when accepted then 1 else 0 end,
	alter COLUMN accepted SET DEFAULT 0;
	exception when others then null;
end;
$$;
