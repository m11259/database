alter table users add column if not exists car_numbers text[];
alter table requests add column if not exists car_number text;

ALTER table Users add COLUMN if not exists lang varchar(2) DEFAULT 'uk';
