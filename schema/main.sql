CREATE TABLE IF NOT EXISTS Users (
    id              bigint          PRIMARY KEY,
    username        varchar(255)    NOT NULL,
    telegramname    varchar(255)    NOT NULL,
    firstname       varchar(255)    NOT NULL DEFAULT '',
    lastname        varchar(255)    ,
    phone           varchar(15)     NOT NULL,
    address	    	varchar(255)    not null default '',
    address_confirmed boolean       not null DEFAULT FALSE,
    car_numbers     text[]          ,
    identified      boolean         DEFAULT FALSE,
    activated       boolean         DEFAULT FALSE,
    created_at      timestamp       DEFAULT now(),
    updated_at      timestamp       NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS Requests (
    id           SERIAL          PRIMARY KEY,
    user_id      integer         REFERENCES Users(id) ON DELETE CASCADE,
    body         text            NOT NULL,
	user_address text			 ,
    car_number   text            , 
    created_at   timestamp       NOT NULL DEFAULT now(),
    accepted     integer         DEFAULT 0,
    reviewed_at  timestamp       
) ;

CREATE TABLE IF NOT EXISTS Admins (
    id          SERIAL          PRIMARY KEY,
    username    text            UNIQUE NOT NULL,
    "password"  text            NOT NULL,
    token       varchar(1000)   NOT NULL UNIQUE,
    created_at  timestamp       NOT NULL DEFAULT now()
);


CREATE TABLE IF NOT EXISTS Sections (
    id          SERIAL          PRIMARY KEY,
	name 		text 			UNIQUE NOT NULL,
	flats_range	integer[2]		NOT NULL
);

CREATE TABLE IF NOT EXISTS Guards (
    id          SERIAL          PRIMARY KEY,
    username    text            UNIQUE NOT NULL,
    "password"  text            NOT NULL,
    token       varchar(1000)   NOT NULL,
	section     text            REFERENCES Sections(name) ON DELETE CASCADE,  
    created_at  timestamp       NOT NULL DEFAULT now()
);


ALTER table Users add COLUMN if not exists lang varchar(2) DEFAULT 'uk';
do $$
begin
	alter table requests rename accepted_at to reviewed_at;
	exception when others then null;
end;
$$;

do $$
begin
	alter table requests      
	alter COLUMN accepted DROP DEFAULT,
	alter COLUMN accepted TYPE integer USING case when accepted then 1 else 0 end,
	alter COLUMN accepted SET DEFAULT 0;
	exception when others then null;
end;
$$;

ALTER TABLE Users ALTER COLUMN username DROP NOT NULL;

ALTER TABLE Requests ADD COLUMN IF NOT EXISTS fulfilled_at timestamp;
DO $$ BEGIN
    CREATE TYPE passCard AS (
    type text,
    id text
);
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

ALTER TABLE Requests ADD COLUMN IF NOT EXISTS pass passCard;

