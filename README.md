# MSG Guard Database & Migrations Service

Database schema and queries

## Deploy
Require:
- Logged in
```bash
docker login registry.gitlab.com
```
Deploy to GitLab
```bash
make
```
