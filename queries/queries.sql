-- name: CreateUser :exec
insert into Users(id, username, telegramname, firstname, lastname, phone, address, address_confirmed, activated, identified, car_numbers, lang)
        values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
        on conflict (id) do update
        set username=$2, telegramname=$3, firstname=$4, lastname=$5, phone=$6, address=$7, address_confirmed=$8, activated=$9, identified=$10, car_numbers=$11, lang=$12, updated_at=now();

-- name: GetUser :one
select * from users where id = $1;

-- name: UnidentifyUser :exec
update users set identified=FALSE where id=$1;

-- name: SetIdentified :exec
update users
        set identified=$2, updated_at=now()
        where id=$1;

-- name: IsIdentified :one
select (identified)
        from users
        where id=$1;

-- name: IsActivated :one
select (activated)
        from users
        where id=$1;

/* -- name: CreateRequest :one */
/* insert into requests(user_id, body, user_address) */
/* 				values ($1, $2, $3) */
/*                 returning *; */

-- name: CreateRequest :one
insert into requests(user_id, body, user_address, car_number)
	values ($1, $2, $3, $4)
    returning *;

-- name: ViewClientRequests :many
select * from requests where user_id=$1 and accepted=0 ORDER BY id LIMIT 3 OFFSET $2;

-- name: DeleteRequest :one
delete from requests where id=$1 returning *;

-- name: GetRequestById :one
select * from requests where id=$1;

-- name: GetRequestUser :one
select users.* 
    from users 
    join requests
    on requests.user_id=users.id
    where requests.id=$1;

-- name: GetUserLang :one
select (lang) from users where id=$1;

-- name: SetUserLang :exec
update users set lang=$2 where id=$1;

